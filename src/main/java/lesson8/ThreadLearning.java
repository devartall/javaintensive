package lesson8;

public class ThreadLearning {
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new TaskExample());
        thread1.setName("Thread_1");
        thread1.setPriority(Thread.MIN_PRIORITY);
        thread1.setDaemon(true);

        Thread thread2 = new Thread(new TaskExample(thread1));
        thread2.setName("Thread_2");
        thread2.setPriority(Thread.MIN_PRIORITY);
        thread2.setDaemon(true);

        Thread thread3 = new Thread(new TaskExample());
        thread3.setName("Thread_3");
        thread3.setPriority(Thread.MIN_PRIORITY);
        thread3.setDaemon(true);

        Thread thread4 = new Thread(new TaskExample());
        thread4.setName("Thread_4");
        thread4.setPriority(Thread.MIN_PRIORITY);
        thread4.setDaemon(true);

        Thread thread5 = new Thread(new TaskExample());
        thread5.setName("Thread_5");
        thread5.setPriority(Thread.MAX_PRIORITY);
        thread5.setDaemon(true);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        System.out.println("Работа основного потока завершена");

        thread1.join(500);
//        thread2.join();
//        thread3.join();
//        thread4.join();
//        thread5.join();

        System.out.println("Работа основного потока завершена");
    }
}

class TaskExample implements Runnable {
    Thread thread;

    public TaskExample(Thread thread1) {
        thread = thread1;
    }

    public TaskExample() {
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1500);
            System.out.println(Thread.currentThread().getName());
            if (thread != null) thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}