package lesson8;

import java.util.concurrent.atomic.AtomicInteger;

public class RaceConditionExample {
    public static AtomicInteger i = new AtomicInteger(0);

    public static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new Task());
        Thread thread2 = new Thread(new Task());
        Thread thread3 = new Thread(new Task());
        Thread thread4 = new Thread(new Task());
        Thread thread5 = new Thread(new Task());

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        thread1.join();
        thread2.join();
        thread3.join();
        thread4.join();
        thread5.join();

        System.out.println(i);
    }
}

class Task implements Runnable {

    @Override
    public void run() {
        for (int j = 0; j < 100000; j++) {
            RaceConditionExample.i.incrementAndGet();
        }
    }
}
