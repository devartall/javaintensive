package lesson8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceExample {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        for (int i = 0; i < 500; i++) {
            executorService.submit(new TaskToExecute());
        }
//        executorService.submit(new TaskToExecute());
//        executorService.submit(new TaskToExecute());
//        executorService.submit(new TaskToExecute());
//        executorService.submit(new TaskToExecute());

        executorService.shutdown();
        executorService.shutdownNow();
        executorService.awaitTermination(1, TimeUnit.SECONDS);
    }
}

class TaskToExecute implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.getClass());
    }
}
